<?php

namespace App\Http\Controllers;

use App\About;
use App\Http\Requests\createAboutRequest;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $show_about = About::all();
        return view('about.index',compact('show_about'));
    }


    public function create()
    {
        return view('about.create');
    }


    public function store(createAboutRequest $request)
    {
        About::create([
            'about' => $request->about,
            'color'=>$request->color,
            'font'=>$request->font
        ]);
        return redirect()->route('About.index');
    }


    public function show(About $about)
    {
        //
    }

    public function edit($about)
    {
        $about_edit = About::findOrFail($about);
        return view('About.edit',compact('about_edit'));
    }


    public function update(Request $request, $about)
    {
        $data = About::findorFail($about)->update([
            'about' => $request->about,
            'color'=>$request->color,
            'font'=>$request->font
        ]);
        return redirect()->route('About.index');
    }


    public function destroy( $about)
    {
        About::where('id',$about)->first()->delete();
        return back();
    }
}
