<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\createContactRequest;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $show_contact = Contact::paginate(5);
        return view('contact.index',compact('show_contact'));
    }

    public function store(createContactRequest $request)
    {
        Contact::create([
            'fullname' => $request->fullname,
            'email'=>$request->email,
            'comment'=>$request->comment,
                ]);
        session()->flash('contact','پیام شما با موفقیت بارگذاری شد');
        return back();
    }


    public function show(Contact $contact)
    {
        //
    }


    public function edit($contact)
    {
        $contact_edit = Contact::findOrFail($contact);
        return view('Contact.edit',compact('contact_edit'));
    }


    public function update(Request $request,$contact)
    {
        $data = Contact::findorFail($contact)->update([
            'status'=>$request->status
        ]);
        return redirect()->route('Contact.index');
    }


    public function destroy($contact)
    {
        Contact::where('id',$contact)->first()->delete();
        session()->flash('delete','عملیات پاک کردن دیتا با موفقیت انجام شد');
        return back();
    }
}
