<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests\createGaleryRequest;
use Illuminate\Http\Request;

class GalleryController extends Controller
{

    public function index()
    {
        $show_gallery = Gallery::all();
        return view('Gallery.index',compact('show_gallery'));
    }

    public function create()
    {
        return view('gallery.create');

    }

    public function store(createGaleryRequest $request)
    {
        $gallery = new Gallery();
        $gallery->name = $request->name;

        $file_gallery = $request->file('image');
        if (!empty($file_gallery)){
            $image=time().$file_gallery->getClientOriginalName();
            $file_gallery->move('images/gallery',$image);
            $gallery->image=$image;
        }

        $gallery->status = $request->status;

        $gallery->save();

        return redirect()->route('Gallery.index');
    }

    public function show(Gallery $gallery)
    {
        //
    }

    public function edit($gallery)
    {
        $gallery_edit = Gallery::findOrFail($gallery);
        return view('Gallery.edit',compact('gallery_edit'));
    }

    public function update(Request $request, $gallery)
    {
        $gallery = Gallery::findOrFail($gallery);
        $gallery->name = $request->name;
        $file_gallery = $request->file('image');
        if (!empty($file_gallery)){
            $imageDelete = $gallery->image;
            unlink("images/gallery/".$imageDelete);
            $image=time().$file_gallery->getClientOriginalName();
            $file_gallery->move('images/gallery/',$image);
            $gallery->image=$image;
        }else{
            $image = $gallery->image;
            $gallery->image=$image;
        }
        $gallery->status = $request->status;
        $gallery->save();
        return redirect()->route('Gallery.index');
    }
    public function destroy($gallery)
    {
        $gallery = Gallery::where('id',$gallery)->first();
        unlink('images/gallery/' . $gallery->image);
        $gallery->delete();
        return back();
    }
}
