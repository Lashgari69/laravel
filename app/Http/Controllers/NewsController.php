<?php

namespace App\Http\Controllers;

use App\Http\Requests\createNewsRequest;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{

    public function index()
    {
        $select = News::all();
        return view('News.index',compact('select'));
    }


    public function create()
    {
        return view('News.create');
    }


    public function store(createNewsRequest $request)
    {
        $news = new News();
        $news->title = $request->title;
        $news->summary = $request->summary;
        $news->description = $request->description;

        $file_news = $request->file('image');
        if (!empty($file_news)){
            $image=time().$file_news->getClientOriginalName();
            $file_news->move('images/news',$image);
            $news->image=$image;
        }

        $news->meta_description = $request->meta_description;
        $news->meta_keywords = $request->meta_keywords;

        if ($request->slug) {
            $news->slug =makeSlug($request->slug);
        }else{
            $news->slug =makeSlug($request->title);
        }

        $news->status = $request->status;

        $news->save();

        return redirect()->route('News.index');
    }

    public function show(News $news)
    {
        //
    }


    public function edit($news)
    {
       $news_edit = News::findOrFail($news);
       return view('News.edit',compact('news_edit'));
    }


    public function update(Request $request, $news)
    {
        $news = News::findOrFail($news);
        $news->title = $request->title;
        $news->summary = $request->summary;
        $news->description = $request->description;
        $file_news = $request->file('image');
        if (!empty($file_news)){
            $imageDelete = $news->image;
            unlink("images/news/".$imageDelete);
            $image=time().$file_news->getClientOriginalName();
            $file_news->move('images/news/',$image);
            $news->image=$image;
        }
        $news->status = $request->status;
        $news->save();
        return redirect()->route('News.index');
    }


    public function destroy($news)
    {
        $news = News::where('id',$news)->first();
        unlink('images/news/' . $news->image);
        $news->delete();
        return back();
    }
}
