<?php

namespace App\Http\Controllers;

use App\Http\Requests\createSettingRequest;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function index()
    {
        $show_setting = Setting::all();
        return view('setting.index',compact('show_setting'));
    }

    public function create()
    {
        return view('setting.create');

    }


    public function store(createSettingRequest $request)
    {
        Setting::create([
            'title' => $request->title,
            'keywords' => $request->keywords,
            'description'=>$request->description,
            'auther'=>$request->auther
        ]);
        return redirect()->route('Setting.index');
    }


    public function show(Setting $setting)
    {
        //
    }

    public function edit($setting)
    {
        $setting_edit = Setting::findOrFail($setting);
        return view('Setting.edit',compact('setting_edit'));
    }


    public function update(Request $request, $setting)
    {
        $data = Setting::findorFail($setting)->update([
            'title' => $request->title,
            'keywords' => $request->keywords,
            'description'=>$request->description,
            'auther'=>$request->auther
        ]);
        return redirect()->route('Setting.index');
    }

    public function destroy($setting)
    {
        Setting::where('id',$setting)->first()->delete();
        return back();
    }
}
