<?php

namespace App\Http\Controllers;

use App\Http\Requests\createSliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Description;

class SliderController extends Controller
{

    public function index()
    {
        $show_slider = Slider::all();
        return view('slider.index',compact('show_slider'));
    }


    public function create()
    {
        return view('slider.create');
    }


    public function store(createSliderRequest $request)
    {

        $slider = new Slider();

        $slider->name=$request->name;

        $file_slider = $request->file('image');
        if (!empty($file_slider)){
            $image=time().$file_slider->getClientOriginalName();
            $file_slider->move('images/slider',$image);
            $slider->image=$image;
        }
        $slider->status=$request->status;
        $slider->save();
        return redirect()->route('Slider.index');

    }



    public function show(Slider $slider)
    {
        //
    }


    public function edit($slider)
    {
        $slider_edit = Slider::findOrFail($slider);
        return view('slider.edit' , compact('slider_edit'));
    }


    public function update(Request $request, $slider)
    {
        $slider=Slider::findOrFail($slider);

        $slider->name=$request->name;

        $file_slider = $request->file('image');
        if (!empty($file_slider)){
            $imageDelete = $slider->image;
            unlink("images/slider/".$imageDelete);
            $image=time().$file_slider->getClientOriginalName();
            $file_slider->move('images/slider',$image);
            $slider->image=$image;
        }
        $slider->status=$request->status;
        $slider->save();

        return redirect()->route('Slider.index');
    }


    public function destroy($slider)
    {
        $slider = Slider::findOrFail($slider);
        unlink('images/slider/' . $slider->image);
        $slider->delete();
        session()->flash('delete','عملیات پاک کردن دیتا با موفقیت انجام شد');
        return back();
    }
}
