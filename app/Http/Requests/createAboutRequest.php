<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createAboutRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'about'=>'required',
            'color'=>'required',
            'font'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'about.required'=>'لطفا در باره ما را پر کنید!',
            'color.required'=>'لطفا رنگ مورد نظر را انتخاب کنید',
            'font.required'=>'لطفا سایز مور نظر خود را انتخاب کنبد',
            ];
    }
}
