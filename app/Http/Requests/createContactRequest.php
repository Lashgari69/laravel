<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createContactRequest extends FormRequest
{


    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fullname'=>'required|min:3|max:50',
            'email'=>'email|required',
            'comment'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'=>'لطفا خود را وارد کنید!',
            'email.required'=>'لطفا ایمیل خود را وارد کنید',
            'email.email'=>'لطفا ایمیل خود را درست وارد کنید',
            'comment.required'=>'لطفا متن مور نظر خود را وارد کنبد',
        ];
    }
}
