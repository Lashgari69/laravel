<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createGaleryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'=>'required|min:3|max:20',
            'image'=>'required|mimes:jpg,jpeg,png|max:1024',
            'status'=>'boolean',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'نام  باید وارد شود',
            'name.min'=>'کاراکتر باید از 3 تا بیشتر باشد',
            'image.required'=>'عکس باید وارد شود',
            'image.mimes'=>'نوع عکس باید jpg, jpeg,png باشد',
            'image.max'=>'سایز عکس نباید بیشتر از 1024 باشد',
            'status.boolean'=>'مقدار 0 یا 1 باید وارد شود',
        ];
    }
}
