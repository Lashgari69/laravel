<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createNewsRequest extends FormRequest
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|min:3|max:50',
            'image'=>'required|mimes:jpg,jpeg,png|max:1024',
            'status'=>'boolean',
            'description'=>'required',
            'slug'=>'required|unique:news'
        ];
    }
    public function messages()
    {
        return [
          'title.required'=>'نام  باید وارد شود',
          'title.min'=>'کاراکتر باید از 3 تا بیشتر باشد',
          'image.required'=>'عکس باید وارد شود',
          'image.mimes'=>'نوع عکس باید jpg, jpeg,png باشد',
          'image.max'=>'سایز عکس نباید بیشتر از 1024 باشد',
          'status.boolean'=>'مقدار 0 یا 1 باید وارد شود',
          'description.required'=>'مقداری برای توضیحات باید وارد شود',
          'slug.required'=>'نباید خالی باشد',
          'slug.unique'=>'مقدار نباید تکراری باشد'
            ];
    }
}
