<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createSettingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|min:3|max:50',
            'keywords'=>'required|min:3|max:50',
            'description'=>'required',
            'auther'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'نام  باید وارد شود',
            'title.min'=>'کاراکتر باید از 3 تا بیشتر باشد',
            'title.max'=>'کاراکتر نباید از 50 تا بیشتر باشد',
            'description.required'=>'مقداری برای توضیحات باید وارد شود',
            'auther.required'=>'نویسنده نباید خالی باشد',

        ];
    }

}
