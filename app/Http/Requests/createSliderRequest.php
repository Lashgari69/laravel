<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createSliderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
                'name'=>'required',
                'image'=>'required|mimes:jpg,jpeg,png|max:1024',
                'status'=>'boolean',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'نام عکس الزامی می باشد',
            'image.required'=>'لطفا عکس را وارد کنید',
            'image.mimes'=>'عکس باید از فرمتهای jpg,jpeg,png باشد!',
            'image.max'=>'حجم عکس نباید از 1024 زیادتر باشد !',
            'status.boolean'=>'وضعیت نمایش باید صفر یا یک باشد!'

        ];
    }
}
