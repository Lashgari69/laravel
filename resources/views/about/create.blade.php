@extends('layouts.layout')
@section('css')
    <title>درباره ما</title>
@endsection
@section('content')
    <section class="container mt-5">
        <section class="col-6 offset-3">
            {{ Form::open(['route'=>'About.store' , 'method'=>'post']) }}


                <section class="form-group">
                    {{ Form::label('about','توضیحات',['class'=>'text-right d-block']) }}

                    {{Form::textarea('about',null,['class'=>'form-control editor' , 'style'=>'border: 2px inset blue; resize: none'])}}

                </section>

            <section class="form-group">
                {{ Form::label('color','',['class'=>'text-right d-block awesome'])}}

                {{ Form::color('color' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{ Form::label('font','',['class'=>'text-right d-block awesome'])}}

                {{ Form::number('font' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>
                {{Form::submit('ذخیره',['class'=>'btn btn-warning btn-block'])}}
            {{ Form::close() }}
        </section>
    </section>
@endsection
