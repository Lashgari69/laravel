@extends('layouts.layout')
@section('css')
    <title>ارتباط با ما</title>

@endsection
@section('content')
    <section class="mt-3 pt-3 text-center">
        <a  class="btn btn-dark text-danger" href="{{ route('admin') }}">Dashbord</a>
        <a  class="btn btn-dark text-danger" href="{{ route('About.create') }}">NEW</a>
    </section>
    <section class="container mt-5">
        <table class="table table-hover table-dark ">
            <thead>
            <tr>
                <th>id</th>
                <th>about</th>
                <th>color</th>
                <th>font</th>
                <th>delete</th>
                <th>update</th>
                <th>created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($show_about as $item)
                <tr>
                    <td>{{ $item->id }} </td>
                    <td>{{ Str::limit($item->about,20) }} </td>
                    <td><h5 style="background-color:{{ $item->color }}">color</h5> </td>
                    <td>{{ $item->font }} </td>

                    <td>
                        {{ Form::open(['route'=>['About.destroy', $item ->id],'method'=>'delete']) }}

                         {{Form::submit('حذف',['class'=>'btn-outline-danger'])}}
                        {{ Form::close() }}
                    </td>
                    <td>
                        {{--
                                            <a href="{{ route('News.edit', $item ->id) }}" >update</a>
                        --}}
                        <form method="GET" action="{{ route('About.edit', $item ->id) }}">
                            @csrf
                            <input type="submit"  class="btn-outline-primary" value="update">
                        </form>
                    </td>
                    <td>{{ \Hekmatinasser\Verta\Verta::instance($item->created_at) }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
@section('js')

@endsection
