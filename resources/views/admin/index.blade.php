@extends('layouts.layout')
@section('css')
    <title>پنل کاربری</title>
@endsection
@section('content')
    <!-- menu -->
    <section class="container-fluid ">
        <section class="menu ">
            <section class="row">
                <section class="col-12 ">
                    <nav class="navbar bg-warning  navbar-expand-sm fixed-top mr-3 ml-3 ">
                        <section class="container-fluid ">
                            <button class="navbar-toggler bg-primary fa fa-list " style="color: #c69500" data-toggle="collapse" data-target="#myToggleNav">

                            </button>
                            <section class="collapse navbar-collapse menuco " id="myToggleNav">
                                <section class="navbar-nav w-100 justify-content-start flex-sm-row-reverse text-center">
                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">تنظیمات</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('Setting.index') }}" class="dropdown-item text-right">نمایش</a>
                                            <a href="{{ route('Setting.create') }}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>
                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">اخبار</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('News.index') }}" class="dropdown-item text-right">نمایش</a>
                                            <a href="{{ route('News.create') }}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>
                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">اسلایدر</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('Slider.index') }}" class="dropdown-item text-right"> نمایش</a>
                                            <a href="{{route('Slider.create')}}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>
                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">گالری</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('Gallery.index') }}" class="dropdown-item text-right"> نمایش</a>
                                            <a href="{{ route('Gallery.create') }}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>
                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">ارتباط با ما</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('Contact.index') }}" class="dropdown-item text-right"> نمایش</a>
                                            <a href="{{ route('Contact.create') }}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>

                                    <section class="dropdown">
                                        <a href="javascript;" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">درباره ما</a>
                                        <section class="dropdown-menu">
                                            <a href="{{ route('About.index') }}" class="dropdown-item text-right"> نمایش</a>
                                            <a href="{{ route('About.create') }}" class="dropdown-item text-right"> اضافه کردن</a>
                                        </section>
                                    </section>

                                        <a href="{{route('show')}}" class="nav-link nav-item-item" target="_blank" >وب سایت</a>


                                    <a class="nav-link nav-item-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('خروج') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </section>
                            </section>
                        </section>

                    </nav>
                </section>

            </section>
        </section>
    </section>
    <!-- end menu -->
    <br><br><br>


    <!-- footer -->

    <section class="container-fluid ">
        <section class="row ">
            <section class=" col-8 offset-2 mt-3">
                <footer class="text-dark text-center">
                    <hr>
                    <p>Copyright &copy; 2019 Blessing.com<p>
                </footer>
            </section>
        </section>
    </section>
    <!-- end footer -->
@endsection
@section('js')

@endsection
