@extends('layouts.layout')
@section('css')
    <title>صفحه ثبت نام</title>
@endsection
@section('content')
    <section class="container-fluid">
        <section class="row">
            <section class="col-8 offset-2 mt-5">
                <header class="text-center text-primary">
                    <h1>Blessing</h1>
                </header>
            </section>
        </section>
    </section>
    <section class="container-fluid">
        <section class="content">
            <section class="row">
                <section class="col-8 offset-2 mt-3 ">
                    <section class=" jumbotron jumcolor ">
                        <section class="text-right">
                            <form action="" method="">
                                <input type="hidden" name="order" value="register">
                                <section class="form-group ">
                                    <label for="username"><h5>نام کاربری </h5></label>
                                    <input type="text" id="username" name="username" class="form-control pr-2" required="required">
                                </section>
                                <section class="form-group ">
                                    <label for="email"><h5>ایمیل </h5></label>
                                    <input type="text" id="email" name="email" class="form-control pr-2" required="required">
                                </section>
                                <section class="form-group ">
                                    <label for="phone"><h5>شماره تماس </h5></label>
                                    <input type="text" id="phone" name="phone" class="form-control pr-2" required="required">
                                </section>
                                <section class="form-group">
                                    <label for="password"><h5>رمز عبور </h5></label>
                                    <input type="text" id="password" name="password" class="form-control pr-2" required="required">
                                </section>

                                <button type="submit" class="btn btn-primary btn-block">ورود</button>
                                <section class=" mt-4 text-center">
                                    <p>قبلا ثبت نام کرده اید؟ <a href="login.php">ورود</a> </p>
                                </section>

                            </form>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
    <hr>
    <section class="container-fluid ">
        <section class="row ">
            <section class=" col-8 offset-2 mt-3">
                <footer class="text-dark text-center">
                    <p>Copyright &copy; 2019 Blessing.com<p>
                </footer>
            </section>
        </section>
    </section>
@endsection
@section('js')

@endsection
