@extends('layouts.layout')
@section('css')
    <title> ارتباط با ما</title>
@endsection
@section('content')
    <section class="container mt-5">
        <section class="col-6 offset-3">
            {{ Form::model($contact_edit,['route'=>['Contact.update' , $contact_edit->id ], 'method'=>'PUT']) }}
            <section class="form-group">
                {{ Form::label('fullname','عنوان',['class'=>'text-right d-block awesome'])}}

                {{ Form::text('fullname' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{ Form::label('comment','توضیحات',['class'=>'text-right d-block']) }}

                {{Form::textarea('comment',null,['class'=>'form-control editor' , 'style'=>'border: 2px inset blue; resize: none'])}}

            </section>

            <section class="form-group">
                {{Form::label('status','وضعیت نمایش',['class'=>'text-right d-block'])}}
                {{Form::number('status',null,['class'=>'form-control', 'style'=>'border: 2px inset blue'])}}
            </section>

            {{Form::submit('ذخیره',['class'=>'btn btn-warning btn-block'])}}
            {{ Form::close() }}
        </section>
    </section>
@endsection
