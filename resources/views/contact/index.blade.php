@extends('layouts.layout')
@section('css')
    <title>ارتباط با ما</title>

@endsection
@section('content')
    <section class="mt-3 pt-3 text-center">
        <a  class="btn btn-dark text-danger" href="{{ route('admin') }}">Dashbord</a>
    </section>
    <section class="text-info text-center " style="direction: rtl">
        برای نمایش و یا عدم نمایش پیام از  update استفاده نمایید .
    </section>
    @if(session('delete'))
        <section class="col-6 offset-3 alert bg-success">
            <h3 class="text-danger text-center">{{ session('delete') }}</h3>
        </section>
    @endif
    <section class="container mt-5">
        <table class="table table-hover table-dark ">
            <thead>
            <tr>
                <th>id</th>
                <th>fullname</th>
                <th>email</th>
                <th>description</th>
                <th>active</th>
                <th>delete</th>
                <th>update</th>
                <th>created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($show_contact as $item)
                <tr>
                    <td>{{ $item->id }} </td>
                    <td>{{ $item->fullname }} </td>
                    <td>{{ $item->email }} </td>
                    <td>{{ Str::limit($item->comment,20) }} </td>
                    <td>
                        @if ( $item->status  == 0)
                            <span class="badge badge-danger">غیرفعال</span>
                        @else
                            <span class="badge badge-success">فعال</span>
                        @endif
                    </td>
                    <td>
                        {{ Form::open(['route'=>['Contact.destroy', $item ->id],'method'=>'delete']) }}

                         {{Form::submit('حذف',['class'=>'btn-outline-danger'])}}
                        {{ Form::close() }}
                    </td>
                    <td>
                        {{--
                                            <a href="{{ route('News.edit', $item ->id) }}" >update</a>
                        --}}
                        <form method="GET" action="{{ route('Contact.edit', $item ->id) }}">
                            @csrf
                            <input type="submit"  class="btn-outline-primary" value="update">
                        </form>
                    </td>
                    <td>{{ \Hekmatinasser\Verta\Verta::instance($item->created_at) }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$show_contact->links()}}
    </section>
@endsection
@section('js')

@endsection
