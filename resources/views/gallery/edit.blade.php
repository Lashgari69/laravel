@extends('layouts.layout')
@section('css')
    <title> گالری تصاویر</title>
@endsection
@section('content')
    <section class="container mt-5">
        <section class="col-6 offset-3 " style="text-align: right">
            {{ Form::model($gallery_edit,['route'=>['Gallery.update' ,$gallery_edit->id], 'method'=>'PUT','files'=>true]) }}
            <section class="form-group">
                {{ Form::label('name','عنوان',['class'=>'text-right d-block '])}}

                {{ Form::text('name' , null ,['class'=>'form-control text-right' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{Form::label('image','عکس',['class'=>'text-right d-block '])}}
                {{Form::file('image',['class'=>'form-control','style'=>'border: 2px inset blue'])}}
                <img src="{{asset('images/gallery/'.$gallery_edit->image)}}" width="50px" height="50px">
            </section>

            <section class="form-group">
                {{Form::label('status','وضعیت نمایش')}}
                {{Form::number('status',null,['class'=>'form-control', 'style'=>'border: 2px inset blue'])}}
            </section>

            <input type="submit" value="save" class="btn btn-warning btn-block">

            {{ Form::close() }}
        </section>
    </section>
@endsection
