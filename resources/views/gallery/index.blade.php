@extends('layouts.layout')
@section('css')
    <title>گالری تصاویر</title>

@endsection
@section('content')
    @if(session('delete'))
        <section class="col-6 offset-3 alert bg-success">
            <h3 class="text-info text-center">{{ session('delete') }}</h3>
        </section>
    @endif
    <section class="mt-3 pt-3 text-center">
        <a  class="btn btn-dark text-danger" href="{{ route('admin') }}">Dashbord</a>
        <a  class="btn btn-dark text-danger" href="{{ route('Gallery.create') }}">NEW</a>
    </section>
    <section class="container mt-5">
        <table class="table table-hover table-dark ">
            <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>image</th>
                <th>status</th>
                <th>delete</th>
                <th>update</th>
                <th>created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($show_gallery as $item)
                <tr>
                    <td>{{ $item->id }} </td>
                    <td>{{ $item->name }} </td>
                    <td><img src="{{asset('images/gallery/'. $item->image) }}" width="50px" height="50px"></td>
                    <td>
                        @if ( $item->status  == 0)
                            <span class="badge badge-danger">غیرفعال</span>
                        @else
                            <span class="badge badge-success">فعال</span>
                        @endif
                    </td>
                    <td>
                        {{ Form::open(['route'=>['Gallery.destroy', $item ->id],'method'=>'delete']) }}

                        {{Form::submit('حذف',['class'=>'btn-outline-danger'])}}

                        {{ Form::close() }}
                    </td>
                    <td>
                        {{--
                                            <a href="{{ route('News.edit', $item ->id) }}" >update</a>
                        --}}
                        <form method="GET" action="{{ route('Gallery.edit', $item ->id) }}">
                            @csrf
                            <input type="submit"  class="btn-outline-primary" value="update">
                        </form>
                    </td>
                    <td>{{ \Hekmatinasser\Verta\Verta::instance($item->created_at) }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
@section('js')

@endsection
