<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css')}}">
    <!--<link rel="stylesheet" href="{{ asset('front/css/bootstrap-reboot.min.css') }}">-->
    <link rel="stylesheet" href="{{ asset('front/css/custom.css') }}">
    @yield('css')
</head>
<body>
@yield('content')

<!-- locate js -->
<script src="{{ asset('front/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{asset('front/js/bootstrap.min.js')}}"></script>
<script src="{{asset('front/js/popper.min.js')}}"></script>
<script src="{{asset('front/js/icon.js')}}"></script>
@yield('js')
</body>
</html>
