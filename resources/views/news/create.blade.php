@extends('layouts.layout')
@section('css')
    <title> ایجاد خبر</title>
@endsection
@section('content')
    @if($errors->any())
        <section class="alert alert-danger col-8 offset-2 mt-5">
            @foreach($errors->all() as $item)
                <h4 class="text-center">{{$item}} </h4>
            @endforeach
        </section>
    @endif
    <section class="container mt-5">
    <section class="col-6 offset-3">
            {{ Form::open(['route'=>'News.store' , 'method'=>'post','files'=>true]) }}
            <section class="form-group">
                <label for="title">title</label>
                <input type="text" id="title" class="form-control" value="" name="title" style="border: 2px inset blue">
            </section>
            <section class="form-group">
                <label for="summary">summary</label>
                <input type="text" id="summary" class="form-control" value="" name="summary" style="border: 2px inset blue">
            </section>
            <section class="form-group">
                {{Form::label('image','عکس',['class'=>'text-right d-block '])}}
                {{Form::file('image',['class'=>'form-control','style'=>'border: 2px inset blue'])}}
            </section>

            <section class="form-group">
                <label for="description">description</label>
                <textarea id="description" class="form-control editor"name="description"  style="border: 2px inset blue; resize: none">
                </textarea>
            </section>
            <section class="form-group">
                <label for="meta_description">meta_description</label>
                <textarea id="meta_description" class="form-control editor"name="meta_description"  style="border: 2px inset blue; resize: none">
                </textarea>
            </section>
            <section class="form-group">
                <label for="meta_keywords">meta_keywords</label>
                <textarea id="meta_keywords" class="form-control editor"name="meta_keywords"  style="border: 2px inset blue; resize: none">
                </textarea>
            </section>
            <section class="form-group">
                <label for="slug">slug</label>
                <textarea id="slug" class="form-control editor"name="slug"  style="border: 2px inset blue; resize: none">
                </textarea>
            </section>
            <section class="form-group">
                <label for="status">status</label>
                <input type="number" id="status" class="form-control" name="status" style="border: 2px inset blue" >
            </section>

            <input type="submit" value="save" class="btn btn-warning btn-block">

        {{ Form::close() }}
    </section>
</section>
@endsection
