@extends('layouts.layout')
@section('css')
    <title> ایجاد خبر</title>
@endsection
@section('content')
    <section class="container mt-5">
        <section class="col-6 offset-3">
            {{ Form::model($news_edit,['route'=>['News.update' ,$news_edit->id], 'method'=>'PUT','files'=>true]) }}

                <section class="form-group">
                    <label for="title">title</label>
                    <input type="text" id="title" class="form-control" value="{{ $news_edit->title }}" name="title" style="border: 2px inset blue">
                </section>
                <section class="form-group">
                    <label for="summary">summary</label>
                    <input type="text" id="summary" class="form-control" value="{{ $news_edit->summary }}" name="summary" style="border: 2px inset blue">
                </section>
                <section class="form-group">
                    {{Form::label('image','عکس',['class'=>'text-right d-block '])}}
                    {{Form::file('image',['class'=>'form-control','style'=>'border: 2px inset blue'])}}
                    <img src="{{asset('images/news/'.$news_edit->image)}}" width="50px" height="50px">
                </section>
                <section class="form-group">
                    <label for="description">description</label>
                    <textarea id="description" class="form-control editor"name="description"  style="border: 2px inset blue; resize: none">{{$news_edit->description}}
                </textarea>
                </section>
                <section class="form-group">
                    <label for="status">status</label>
                    <input type="number" id="status" class="form-control" name="status" value="{{$news_edit->status}}" style="border: 2px inset blue" >
                </section>
                <input type="submit" value="save" class="btn btn-warning btn-block">

            {{ Form::close() }}
        </section>
    </section>
@endsection
