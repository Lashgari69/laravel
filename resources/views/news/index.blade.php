@extends('layouts.layout')
@section('css')
    <title>خبر ها</title>

@endsection
@section('content')
    <section class="mt-3 pt-3 text-center">
        <a  class="btn btn-dark text-danger" href="{{ route('admin') }}">Dashbord</a>
        <a  class="btn btn-dark text-danger" href="{{ route('News.create') }}">NEW</a>
    </section>
    <section class="container mt-5">
        <table class="table table-hover table-dark ">
            <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>image</th>
                <th>summary</th>
                <th>active</th>
                <th>delete</th>
                <th>update</th>
                <th>created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($select as $item)
            <tr>
                <td>{{ $item->id }} </td>
                <td>{{ $item->title }} </td>
                <td><img src="{{asset('images/news/'. $item->image) }}" width="50px" height="50px"></td>
                <td>{{ $item->summary }} </td>
                <td>
                    @if ( $item->status  == 0)
                    <span class="badge badge-danger">غیرفعال</span>
                    @else
                    <span class="badge badge-success">فعال</span>
                    @endif
                </td>
                <td>
                    <form method="post" action="{{ route('News.destroy', $item ->id) }}">
                        @csrf
                        @method('delete')
                        <input type="submit"  class="btn-outline-danger" value="delete">
                    </form>
                </td>
                <td>
{{--
                    <a href="{{ route('News.edit', $item ->id) }}" >update</a>
--}}
                    <form method="GET" action="{{ route('News.edit', $item ->id) }}">
                        @csrf
                        <input type="submit"  class="btn-outline-primary" value="update">
                    </form>
                </td>
                <td>{{ \Hekmatinasser\Verta\Verta::instance($item->created_at) }} </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
@section('js')

@endsection
