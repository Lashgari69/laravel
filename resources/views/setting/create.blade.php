@extends('layouts.layout')
@section('css')
    <title> تنظیماتا</title>
@endsection
@section('content')
    @if($errors->any())
        <section class="alert alert-danger col-8 offset-2 mt-5">
            @foreach($errors->all() as $item)
                <h4 class="text-center">{{$item}} </h4>
            @endforeach
        </section>
    @endif
    <section class="container mt-5">
        <section class="col-6 offset-3">
            {{ Form::open(['route'=>'Setting.store' , 'method'=>'post']) }}
                <section class="form-group">
                   {{ Form::label('title','عنوان',['class'=>'text-right d-block awesome'])}}

                    {{ Form::text('title' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

                </section>

                <section class="form-group">
                   {{ Form::label('keywords','کلمات کلیدی',['class'=>'text-right d-block awesome'])}}

                    {{ Form::text('keywords' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

                </section>

                <section class="form-group">
                    {{ Form::label('description','توضیحات',['class'=>'text-right d-block']) }}

                    {{Form::textarea('description',null,['class'=>'form-control editor' , 'style'=>'border: 2px inset blue; resize: none'])}}

                </section>

                <section class="form-group">
                    {{ Form::label('auther','نویسنده',['class'=>'text-right d-block awesome'])}}

                    {{ Form::text('auther' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

                </section>


                {{Form::submit('ذخیره',['class'=>'btn btn-warning btn-block'])}}
            {{ Form::close() }}
        </section>
    </section>
@endsection
