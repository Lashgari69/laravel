@extends('layouts.layout')
@section('css')
    <title>تنظیماتا</title>
@endsection
@section('content')
    <section class="container mt-5">
        <section class="col-6 offset-3">
            {{ Form::model($setting_edit,['route'=>['Setting.update' , $setting_edit->id ], 'method'=>'PUT']) }}
            <section class="form-group">
                {{ Form::label('title','عنوان',['class'=>'text-right d-block awesome'])}}

                {{ Form::text('title' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{ Form::label('keywords','کلمات کلیدی',['class'=>'text-right d-block awesome'])}}

                {{ Form::text('keywords' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{ Form::label('description','توضیحات',['class'=>'text-right d-block']) }}

                {{Form::textarea('description',null,['class'=>'form-control editor' , 'style'=>'border: 2px inset blue; resize: none'])}}

            </section>

            <section class="form-group">
                {{ Form::label('auther','نویسنده',['class'=>'text-right d-block awesome'])}}

                {{ Form::text('auther' , null ,['class'=>'form-control' , 'style'=>'border: 2px inset blue'])}}

            </section>

            {{Form::submit('ذخیره',['class'=>'btn btn-warning btn-block'])}}
            {{ Form::close() }}
        </section>
    </section>
@endsection
