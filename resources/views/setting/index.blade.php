@extends('layouts.layout')
@section('css')
    <title>تنظیمات</title>

@endsection
@section('content')
    <section class="mt-3 pt-3 text-center">
        <a  class="btn btn-dark text-danger" href="{{ route('admin') }}">Dashbord</a>
        <a  class="btn btn-dark text-danger" href="{{ route('Setting.create') }}">NEW</a>
    </section>
    <section class="container mt-5">
        <table class="table table-hover table-dark ">
            <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>keywords</th>
                <th>discription</th>
                <th>auther</th>
                <th>delete</th>
                <th>update</th>
                <th>created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($show_setting as $item)
                <tr>
                    <td>{{ $item->id }} </td>
                    <td>{{ $item->title }} </td>
                    <td>{{ $item->keywords }} </td>
                    <td>{{ Str::limit($item->description , 10) }} </td>
                    <td>{{ $item->auther }} </td>

                    <td>
                        {{ Form::open(['route'=>['Setting.destroy', $item ->id],'method'=>'delete']) }}

                         {{Form::submit('حذف',['class'=>'btn-outline-danger'])}}
                        {{ Form::close() }}
                    </td>
                    <td>
                        {{--
                                            <a href="{{ route('News.edit', $item ->id) }}" >update</a>
                        --}}
                        <form method="GET" action="{{ route('Setting.edit', $item ->id) }}">
                            @csrf
                            <input type="submit"  class="btn-outline-primary" value="update">
                        </form>
                    </td>
                    <td>{{ \Hekmatinasser\Verta\Verta::instance($item->created_at) }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection
@section('js')

@endsection
