@extends('layouts.layout')
@section('css')
    <title> ارتباط با ما</title>
@endsection
@section('content')
    @if($errors->any())
        <section class="alert alert-danger col-8 offset-2 mt-5">
            @foreach($errors->all() as $item)
                <h4 class="text-center">{{$item}} </h4>
            @endforeach
        </section>
    @endif
    <section class="container mt-5">
        <section class="col-6 offset-3 " style="text-align: right">
            {{ Form::open(['route'=>'Slider.store' , 'method'=>'post','files'=>true]) }}
            <section class="form-group">
                {{ Form::label('name','عنوان',['class'=>'text-right d-block '])}}

                {{ Form::text('name' , null ,['class'=>'form-control text-right' , 'style'=>'border: 2px inset blue'])}}

            </section>

            <section class="form-group">
                {{Form::label('image','عکس',['class'=>'text-right d-block '])}}
                {{Form::file('image',['class'=>'form-control','style'=>'border: 2px inset blue'])}}
            </section>

            <section class="form-group">
                {{Form::label('status','وضعیت نمایش')}}
                {{Form::number('status',null,['class'=>'form-control', 'style'=>'border: 2px inset blue'])}}
            </section>

            <input type="submit" value="save" class="btn btn-warning btn-block">

            {{ Form::close() }}
        </section>
    </section>
@endsection
