@include('partial._header')
<!--main website-->
<main class="container-fluid pr-0 pl-0">
    <!--make menu-->
    @include('partial._menu')
    <!--end make menu-->
    <!--make slider-->
    @include('partial._slider')
    <!--end make slider-->
    <!--make about-->
    @include('partial._about')
    <!--end make about-->
    <!--make gallery-->
    @include('partial._gallery')
    <!--end make gallery-->
    <!--make contact-->
    @include('partial._contact')
    <!--end make contact-->
    <!--make footer-->
    @include('partial._footerMain')
    <!--end make footer-->
</main>
<!--end main website-->
@include('partial._footer')
