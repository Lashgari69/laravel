<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('show');



Auth::routes();

Route::middleware('auth')->prefix('administrator')->group(function (){
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::resource('/Contact','ContactController');
    Route::resource('/Gallery','GalleryController');
    Route::resource('/News','NewsController');
    Route::resource('/Slider','SliderController');
    Route::resource('/Setting','SettingController');
    Route::resource('/About','AboutController');
});

Route::post('/insertContact','ContactController@store')->name('Contact.Data');
